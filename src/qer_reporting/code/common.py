import os
import numpy as np
import SimpleITK as sitk
import jinja2
from PIL import Image

HERE = os.path.dirname(__file__)
FONT_PATH = os.path.join(HERE, '../data/Andale Mono.ttf')
AVENIR = os.path.join(HERE, '../data/AvenirLTStd-Book.otf')
AVENIR_MEDIUM = os.path.join(HERE, '../data/AvenirLTStd-Medium.otf')
CAUTION_SYMBOL = Image.open(os.path.join(HERE, '../data/caution_high_res.png'))
BULLET_POINT = Image.open(os.path.join(HERE, '../data/marker_high_res.png'))
SYMBOL_FONT_PATH = os.path.join(HERE, '../data/DejaVuSans.ttf')
LOGO_PATH = os.path.join(HERE, '../data/logo_color_low_res.png')
LOGO = Image.open(LOGO_PATH)
LOGO_WHITE = Image.open(os.path.join(HERE, '../data/logo_white.png'))
DCM_SR_REPORT_PATH = os.path.join(HERE, '../data/sr_report_template.dcm')
HL7_TEMPLATE_PATH = os.path.join(HERE, '../data/template.hl7')

COLOR_LIBRARY = [
    (255, 37, 17),    # red
    (34, 139, 34),    # green
    (255, 215, 0),    # yellow
    (0, 255, 255),    # cyan
    (255, 0, 255)     # magenta
]

SPECIAL_COLORS = {
    'fracture': (255, 0, 0),                         # bright red
    'intracranial hemorrhage': (229, 59, 59),        # red
    'intraparenchymal hemorrhage': (229, 119, 59),   # orange
    'subdural hemorrhage': (0, 255, 229),            # cyan
    'intraventricular hemorrhage': (127, 229, 59),   # lime green
    'subarachnoid hemorrhage': (229, 226, 59),       # yellowish
    'extradural hemorrhage': (127, 59, 229),         # blue
    'infarct': (33, 186, 97)                         # dark green
}

def get_report_template(template):
    template_path = os.path.join(HERE, '../data/{}.html'.format(template))
    return jinja2.Template(open(template_path).read())

def _get_image_from_array(pixel_array):
    pixel_array = (pixel_array - pixel_array.min()) / \
        (pixel_array.max() - pixel_array.min())
    pixel_array = (255 * pixel_array).astype('uint8')
    image = Image.fromarray(pixel_array).convert('RGB')
    return image


def _get_thumbnail_from_dicom(dicom_path):
    overlay = sitk.GetArrayFromImage(sitk.ReadImage(dicom_path))[0]
    overlay = np.clip(overlay, 0, 80) / 80   # brain window
    return _get_image_from_array(overlay)


def get_path_colors(path_mask_dict):
    path_colors = {}
    unknown_ind = 0
    for path, mask in path_mask_dict.items():
        if mask is not None:
            try:
                color = [v for k, v in SPECIAL_COLORS.items()
                         if k in path.lower()][0]
            except IndexError:
                color = COLOR_LIBRARY[unknown_ind]
                unknown_ind += 1

            path_colors[path] = color

    return path_colors