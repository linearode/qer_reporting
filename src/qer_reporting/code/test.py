import json 
from pdf_report import preprocess

f = open('pdf_report_options.json')
pdf_report_options = json.load(f)
f = open('sample_data.json')
result_json = json.load(f)

patient_metadata = {
    'institution': "InstitutionName",
    'study_date': "study_date",
    'study_id_dcm_tag': "getattr(plan, 'StudyID', None)",
    'study_date_dcm_tag': "getattr(plan, 'StudyDate', None)",
    'study_date_time': "study_date_time",
    'patient_id': "getattr(plan, 'PatientID', None)",
    'accession_number': "getattr(plan, 'AccessionNumber', None)",
    'analysis_date_time': "now.strftime('%d %b %Y %X')"
}


preprocess(result_json, patient_metadata, pdf_report_options)