class PDFMargin:
    def __init__(self, options):

        try:
            self.top = options.layout.margin.top
        except AttributeError:
            self.top = 0.5

        try:
            self.bottom = options.layout.margin.bottom
        except AttributeError:
            self.bottom = 0.5


class PDFLayout:
    def __init__(self, options):

        try:
            self.font_size = options.layout.font_size
        except AttributeError:
            self.font_size = 10

        self.margin = PDFMargin(options)


class PDFOptions:
    """
    Configurable options in Secondary Capture generation
    """

    def __init__(self, options):
        # PDF layout configuration
        self.layout = PDFLayout(options)

        # PDF language
        self.language = options['lang']

        # Triage status
        self.triage_status = options['triage_status']

        # Abnormality decisions
        self.decisions = options['decisions']

        # Impression
        self.impressions = options['impression'].split("\n")

        # QER version
        self.qer_version = options['qer_version']

        # Study Metadata
        self.metadata = options['metadata']

        # Template too be used
        self.template = options.get('template', 'hct_report_template')

        # Save path
        self.save_path = options['save_path']

        # FDA UDI option
        self.add_fda_udi = options.get('add_fda_udi', 'False')

        # private tag options
        self.private_bnb_tag = options.get('private_bnb_tag', {})



