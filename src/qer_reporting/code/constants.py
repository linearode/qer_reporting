CAUTION_HEAD = 'NOT FOR DIAGNOSTIC USE'
CAUTION_BODY = 'Caution: qER Is intended for use by medical specialists qualified to interpret head CT scans.\nqER is not a diagnostic device and treatment should not be initiated based on the results.\nThe clinician is responsible for reviewing the CT scan as per the standard of care.'
ARTIFACT_WARNING = 'Image artifact is detected, qER results may be inaccurate'
EMPTY_1 = '[__,__.__.__._]'
EMPTY_2 = '[__,__.__.__.__]'
EMPTY_3 = '[__.__.__.__]'
ARTIFACT = 'artifact'
FDA_UDI = 'UDI: (01)00860005118106(10)2.0.0'
NORMAL_REPORT = 'No intracranial hemorrhage found.'
NEW_SLICE_FIELDS = ['InstanceNumber', 'SOPInstanceUID', 'ImagePositionPatient',
                    'SliceLocation']
MAX_THUMBNAILS = 40
GREY = (80, 80, 80)
RED = (255, 0, 0)
B_RED = (225, 48, 38)
WHITE = (255, 255, 255)


class DisplayTexts:
    SUMMARY_DISPLAY_INVALID_HEADER = 'sc_summary_display_invalid_error'
    SUMMARY_DISPLAY_ABNORMALITIES_BULLETTED_HEADER = 'sc_summary_abnormalities_header'
    SUMMARY_DISPLAY_CAUTION_HEADER = 'sc_summary_caution_header'
    SUMMARY_DISPLAY_CAUTION_BODY = 'sc_summary_caution_body'
    SUMMARY_DISPLAY_ANALYSIS_PERFORMED_TIME = 'sc_summary_analysis_performed_time'
    PDF_REPORT_WARNING_MSG = 'PDF_REPORT_WARNING_MSG'
    PDF_REPORT_WARNING = 'PDF_REPORT_WARNING'
    PDF_REPORT_PATIENT_NAME = 'PDF_REPORT_PATIENT_NAME'
    PDF_REPORT_PATIENT_ID = 'PDF_REPORT_PATIENT_ID'
    PDF_REPORT_SCAN_DATE = 'PDF_REPORT_SCAN_DATE'
    PDF_REPORT_ANALYSIS_DATE = 'PDF_REPORT_ANALYSIS_DATE'
    PDF_REPORT_QER_VERSION = 'PDF_REPORT_QER_VERSION'
    PDF_REPORT_IMPRESSION = 'PDF_REPORT_IMPRESSION'
    PDF_REPORT_SLICE_PREVIEW = 'PDF_REPORT_SLICE_PREVIEW'
    PDF_REPORT_SLICE = 'PDF_REPORT_SLICE'
    PDF_REPORT_CRITICAL_ABNORMALITY = 'PDF_REPORT_CRITICAL_ABNORMALITY'
    PDF_REPORT_PRESENCE = 'PDF_REPORT_PRESENCE'
    PDF_REPORT_VOLUME = 'PDF_REPORT_VOLUME'
    PDF_REPORT_TITLE = 'PDF_REPORT_TITLE'

    Pathologies_ICH = 'Intracranial hemorrhage',
    Pathologies_ACUTE_ICH = 'Acute intracranial hemorrhage',
    Pathologies_IPH = 'Intraparenchymal hemorrhage',
    Pathologies_SDH = 'Subdural hemorrhage',
    Pathologies_EDH = 'Extradural hemorrhage',
    Pathologies_SAH = 'Subarachnoid hemorrhage',
    Pathologies_IVH = 'Intraventricular hemorrhage',
    Pathologies_INFARCT = 'Hypodensity suggestive of a chronic infarct',
    Pathologies_ARTIFACT = (
        'Significant artifacts were detected in this scan; '
        'this report might not be accurate'),
    Pathologies_FRACTURE = 'Cranial fracture',
    Pathologies_ME = 'Mass effect',
    Pathologies_MLS = 'Midline shift',
    Pathologies_PNEUMOCEPHALUS = 'Pneumocephalus',
    Pathologies_ATROPHY = 'Diffuse cerebral atrophy'