"""Create Secondary capture for Head CT."""
import numpy as np
import os
import pydicom
import SimpleITK as sitk
import subprocess
import shutil
import weasyprint
import tempfile
from datetime import datetime
from munch import unmunchify

from qure_series_classifier import sort_dicoms
from common import get_report_template, LOGO_PATH, HERE, _get_thumbnail_from_dicom
from language import Translations
from pdf_options import PDFOptions


def encapsulate_pdf(pdf, orig_dcm_path, pdf_options):
    """Encapsulate pdf to dicom.

    Args:
        pdf(bytes): PDF file read.
        orig_dcm_path(str): Original dicom path.
        save_path(str): Paths for secondary capture image.
    """
    assert isinstance(pdf, bytes)
    save_path = pdf_options.save_path
    with tempfile.TemporaryDirectory() as tmpdirname:
        pdf_path = os.path.join(tmpdirname, 'report.pdf')
        open(pdf_path, 'wb').write(pdf)
        subprocess.run(['pdf2dcm',
                        '--study-from', orig_dcm_path,
                        '--title', 'qER report',
                        pdf_path, save_path], check=True)

        now = datetime.now()
        metadata = pdf_options.metadata
        tags = {
            'ProtocolName': 'qER Analysis',
            'SeriesDescription': 'qER Report',
            'Manufacturer': 'Qure.ai',
            'SeriesDate': now.strftime('%Y%m%d'),
            'SeriesTime': now.strftime('%H%M%S'),
            'AcquisitionDate': now.strftime('%Y%m%d'),
            'AcquisitionTime': now.strftime('%H%M%S'),
            'AccessionNumber': metadata['accession_number'],
            'StudyDate': metadata['study_date_dcm_tag'],
            'StudyID': metadata['study_id_dcm_tag'],
            'SeriesNumber': 4001,
            'ManufacturerModelName': 'QER',
            'SoftwareVersions': pdf_options.qer_version
        }

        plan = pydicom.read_file(save_path)
        for k, v in tags.items():
            setattr(plan, k, v)
        try:
            if pdf_options.add_fda_udi:
                ds = pydicom.Dataset()
                ds.UniqueDeviceIdentifier = "(01)00860005118106(10)2.0.0"

                setattr(plan, 'UDISequence', pydicom.sequence.Sequence([ds]))

            private_nba_tag = pdf_options.private_bnb_tag

            add_private_nba_tag = private_nba_tag.get('add', False)

            if add_private_nba_tag:

                group = private_nba_tag.get('group', 4919)
                header = private_nba_tag.get('header', "Qure")

                if private_nba_tag.get('get_pathology', False):
                    text = "NO FINDINGS"
                    for k in decisions:
                        pathology = private_nba_tag.get('pathology', '')
                        report_lang = pdf_options.lang
                        translations = Translations()
                        pat = translations.translate_pathology(pathology, report_lang)
                        if k == pat:
                            text = "FINDINGS" if decisions[k]['decision'] else "NO FINDINGS"
                            break

                else:
                    triage = pdf_options.triage_status  # fetch hemorrhage classifier information
                    if triage == "Routine":
                        text = "NO FINDINGS"
                    else:
                        text = "FINDINGS"
                if group is not None:
                    block = plan.private_block(int(group), header, create=True)
                    block.add_new(0x01, "CS", text)
                    additional_private_tag = private_nba_tag.get('additional', False)
                    if additional_private_tag:
                        additional_header = private_nba_tag.get('additional_header', 'AI_RESULTS_02')
                        block = plan.private_block(int(group), additional_header, create=True)
                        block.add_new(0x01, "CS", text)
                        block.add_new(0x02, "CS", "HEADCT ABNORMALITIES DETECTION")

        except:
            pass

        pydicom.write_file(save_path, plan)
        return plan


def _get_important_slices(path_mask_dict):
    all_volumes = {k: v for k, v in path_mask_dict.items() if v is not None}
    if len(all_volumes) == 0:
        return None

    slice_wise_volumes = {k: sitk.GetArrayFromImage(v).sum(axis=(1, 2))
                          for k, v in all_volumes.items()}

    top_slices = {k: np.argsort(v)[-3:]
                  for k, v in slice_wise_volumes.items()}
    top_slices = {k: [x for x in v if slice_wise_volumes[k][x] > 0]
                  for k, v in top_slices.items()}

    all_top_slices = sorted({j for i in top_slices.values() for j in i})
    if len(all_top_slices) < 3:
        return None

    middle = round((len(all_top_slices) - 1) / 2)
    all_top_slices = all_top_slices[middle - 1: middle + 2]
    display = [{'slice_num': int(x),
                'caption': ', '.join(k for k, v in slice_wise_volumes.items()
                                     if v[x] > 0)}
               for x in all_top_slices]
    return display


def _get_report_metadata(dicom_series):
    plan = pydicom.read_file(dicom_series[0])

    now = datetime.now()
    try:
        study_date = datetime.strptime(plan.StudyDate,
                                       '%Y%m%d')  # YYYYMMDD
        study_date = study_date.strftime('%d %b %Y')
    except:
        study_date = None

    try:
        study_date_time = datetime.strptime(plan.StudyDate + plan.StudyTime,
                                            '%Y%m%d%H%M%S')  # YYYYMMDD
        study_date_time = study_date.strftime('%d %b %Y %X')
    except:
        study_date_time = study_date

    metadata_dict = {
        'institution': getattr(plan, 'InstitutionName', None),
        'study_date': study_date,
        'study_id_dcm_tag': getattr(plan, 'StudyID', None),
        'study_date_dcm_tag': getattr(plan, 'StudyDate', None),
        'study_date_time': study_date_time,
        'patient_id': getattr(plan, 'PatientID', None),
        'accession_number': getattr(plan, 'AccessionNumber', None),
        'analysis_date_time': now.strftime('%d %b %Y %X')
    }

    return metadata_dict


def create_pdf_report(dicom_series, path_mask_dict, pdf_options):
    with tempfile.TemporaryDirectory() as tmpdirname:

        # important_slices = _get_important_slices(path_mask_dict)
        important_slices = None
        # create PNGs for important slices
        if important_slices is not None:
            for important_slice in important_slices:
                slice_num = important_slice['slice_num']
                thumbnail = _get_thumbnail_from_dicom(dicom_series[slice_num])
                thumbnail_path = os.path.join(
                    tmpdirname, 'thumbnail_{}.png'.format(slice_num))
                thumbnail.save(thumbnail_path)
                important_slice['thumbnail_path'] = thumbnail_path
        else:
            important_slices = list()

        try:
            report_template_format = pdf_options.template
            report_lang = pdf_options.language
        except Exception as e:
            report_template_format = 'hct_report_template'
            report_lang = 'EN'

        report_template = get_report_template(report_template_format)
        translations = Translations()
        place_holder = translations.get_translated_dictionary(report_lang)
        html = report_template.render(pdf_options=pdf_options,
                                      important_slices=important_slices,
                                      place_holder=place_holder)

        html_path = os.path.join(tmpdirname, 'report.html')
        pdf_path = os.path.join(tmpdirname, 'report.pdf')
        open(html_path, 'w').write(html)

        # copy logo
        shutil.copy(LOGO_PATH, tmpdirname)
        shutil.copytree(os.path.join(HERE, '../data/static/'),
                        os.path.join(tmpdirname, 'static/'))

        weasy = weasyprint.HTML(filename=html_path)
        weasy.write_pdf(pdf_path, zoom=1.5)
        return open(pdf_path, 'rb').read()


def create_encapsulated_pdf_report(metadata, dicom_series, path_mask_dict, pdf_report_options):
    """Create encapsulated pdf report."""
    # dicom_series = sort_dicoms(dicom_series)
    # metadata = _get_report_metadata(dicom_series)
    print("here")
    pdf_report_options['metadata'] = metadata

    pdf_options = PDFOptions(pdf_report_options)
    pdf = create_pdf_report(dicom_series, path_mask_dict, pdf_options)
    plan = encapsulate_pdf(pdf, dicom_series[0], pdf_options)
    return plan

def preprocess(result_json, patient_metadata, pdf_report_options):
    print("here")
    dicom_series = result_json["files"]["sc"]
    path_mask_dict = result_json["path_mask_dict"]
    return create_encapsulated_pdf_report(patient_metadata, dicom_series, path_mask_dict, pdf_report_options)
    
