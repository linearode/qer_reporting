from constants import DisplayTexts
# from qer_utils.constants import Pathologies


class Translations:

    def __init__(self):
        self.DICTIONARY = {
            'EN': {
                DisplayTexts.SUMMARY_DISPLAY_INVALID_HEADER: 'Rejected due to following reason',
                DisplayTexts.SUMMARY_DISPLAY_ABNORMALITIES_BULLETTED_HEADER: 'Please review for the following abnormalities',
                DisplayTexts.SUMMARY_DISPLAY_CAUTION_HEADER: 'NOT FOR DIAGNOSTIC USE',
                DisplayTexts.SUMMARY_DISPLAY_CAUTION_BODY: 'Caution: qER Is intended for use by medical specialists'\
                    'qualified to interpret head CT scans.\nqER is not a diagnostic device and treatment should'\
                    'not be initiated based on the results.\nThe clinician is responsible for reviewing the CT scan'\
                    'as per the standard of care.',
                DisplayTexts.SUMMARY_DISPLAY_ANALYSIS_PERFORMED_TIME: 'Analysis performed at',
                DisplayTexts.PDF_REPORT_WARNING_MSG: "This is a preliminary report of head CT scan findings by automated analysis." \
                                          "It is notintended for standalone diagnostic use. Please refer to a radiologist or neurologist for diagnosis.",
                DisplayTexts.PDF_REPORT_WARNING: "Warning",
                DisplayTexts.PDF_REPORT_PATIENT_NAME: "Patient Name",
                DisplayTexts.PDF_REPORT_PATIENT_ID: "Patient ID",
                DisplayTexts.PDF_REPORT_SCAN_DATE: "Scan date",
                DisplayTexts.PDF_REPORT_ANALYSIS_DATE: "Analysis date",
                DisplayTexts.PDF_REPORT_QER_VERSION: "qER version",
                DisplayTexts.PDF_REPORT_IMPRESSION: "Impression",
                DisplayTexts.PDF_REPORT_SLICE_PREVIEW: "Key Preview Slices",
                DisplayTexts.PDF_REPORT_SLICE: "slice",
                DisplayTexts.PDF_REPORT_CRITICAL_ABNORMALITY: "Critical Abnormality",
                DisplayTexts.PDF_REPORT_PRESENCE: "Presence",
                DisplayTexts.PDF_REPORT_VOLUME: "Size/Volume",
                DisplayTexts.PDF_REPORT_TITLE: "qER Automated head CT scan analysis",
            },
            'FR': {
                DisplayTexts.SUMMARY_DISPLAY_INVALID_HEADER: 'Rejeté pour la raison suivante',
                DisplayTexts.SUMMARY_DISPLAY_ABNORMALITIES_BULLETTED_HEADER: 'Veuillez examiner les anomalies suivantes',
                DisplayTexts.SUMMARY_DISPLAY_CAUTION_HEADER: 'Non destiné à des fins diagnostiques',
                DisplayTexts.SUMMARY_DISPLAY_CAUTION_BODY: "Attention : qER est destiné à être utilisé par des professionnels de"\
                    "santé qualifiés dans l'interprétation des scanners crâniens. qER n'est pas un dispositif de"\
                    "diagnostic et la prise en charge ne doit pas être initiée sur la base de ses résultats."\
                    "Le praticien est seul responsable de l'examen de TDM.",
                DisplayTexts.SUMMARY_DISPLAY_ANALYSIS_PERFORMED_TIME: 'Analyse réalisée à',
                DisplayTexts.PDF_REPORT_WARNING_MSG: "Ceci est un rapport préliminaire généré automatiquement. Il n’est pas destiné à " \
                                          "être utilisé seul. Seul le rapport radiologique fait foi.",
                DisplayTexts.PDF_REPORT_WARNING: "Attention",
                DisplayTexts.PDF_REPORT_PATIENT_NAME: "Nom Patient",
                DisplayTexts.PDF_REPORT_PATIENT_ID: "ID Patient",
                DisplayTexts.PDF_REPORT_SCAN_DATE: "Date Scan",
                DisplayTexts.PDF_REPORT_ANALYSIS_DATE: "Date Analyse",
                DisplayTexts.PDF_REPORT_QER_VERSION: "Version qER",
                DisplayTexts.PDF_REPORT_IMPRESSION: "Résultat",
                DisplayTexts.PDF_REPORT_SLICE_PREVIEW: "Coupes Représentatives",
                DisplayTexts.PDF_REPORT_SLICE: "Coupe",
                DisplayTexts.PDF_REPORT_CRITICAL_ABNORMALITY: "Anomalies critiques",
                DisplayTexts.PDF_REPORT_PRESENCE: "Présence",
                DisplayTexts.PDF_REPORT_VOLUME: "Taille / Volume",
                DisplayTexts.PDF_REPORT_TITLE: "qER - Analyse automatique de TDM crânien",
            }
        }

        self.PATHALOGY_DISPLAY = {
            'EN': {
                'ICH': 'Intracranial hemorrhage',
                'ACUTE_ICH': 'Acute intracranial hemorrhage',
                'IPH': 'Intraparenchymal hemorrhage',
                'SDH': 'Subdural hemorrhage',
                'EDH': 'Extradural hemorrhage',
                'SAH': 'Subarachnoid hemorrhage',
                'IVH': 'Intraventricular hemorrhage',
                'INFARCT': 'Hypodensity suggestive of a chronic infarct',
                'ARTIFACT': (
                    'Significant artifacts were detected in this scan; '
                    'this report might not be accurate'),
                'FRACTURE': 'Cranial fracture',
                'ME': 'Mass effect',
                'MLS': 'Midline shift',
                'PNEUMOCEPHALUS': 'Pneumocephalus',
                'ATROPHY': 'Diffuse cerebral atrophy'
            },
            'FR': {
                'ICH': 'Hémorragie intracérébrale',  # Provided by Incepto
                'ACUTE_ICH': 'Hémorragie intracérébrale aiguë',  # Provided by Incepto
                'IPH': 'Hémorragie intraparenchymateuse',
                'SDH': 'Hémorragie sous-durale',
                'EDH': 'Hémorragie extradurale',
                'SAH': 'Hémorragie sous-arachnoïdienne',
                'IVH': 'Hémorragie intraventriculaire',
                'INFARCT': 'Hypodensité suggérant un infarctus',
                'Pathologies_ARTIFACT': (
                    "Artefacts importants détéctés dans l'image; "  # Provided by Incepto
                    "risque d'erreurs dans ce rapport"),  # Provided by Incepto
                'FRACTURE': 'Fracture crânienne',
                'ME': 'Effet de masse',
                'MLS': 'Décalage de la ligne médiane',
                'PNEUMOCEPHALUS': 'Pneumocephalus',
                'ATROPHY': 'Atrophie cérébrale diffuse'
            }
        }

    def translate(self, key, language):
        try:
            return self.DICTIONARY[language][key]
        except KeyError:
            raise RunTimeError(f'Translation not found for {key} in {language}')

    def get_translated_dictionary(self, language):
        try:
            return self.DICTIONARY[language]
        except KeyError:
            raise RunTimeError(f'Translation not found for {language}')

    def translate_pathology(self, key, language):
        try:
            return self.PATHALOGY_DISPLAY[language][key]
        except KeyError:
            raise RunTimeError(f'Translation not found for {key} in {language}')